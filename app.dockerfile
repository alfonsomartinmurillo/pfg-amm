FROM openjdk:8-jdk-alpine
MAINTAINER amartin183@alumno.uned.es
COPY binaries/uned-educa-analisis.jar uned-educa-analisis.jar
ENTRYPOINT ["java","-jar","/uned-educa-analisis.jar"]
