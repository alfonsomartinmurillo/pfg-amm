

[TOC]

## Ubicación del proyecto
En el repositorio https://bitbucket.org/alfonsomartinmurillo/pfg-amm se encuentra custodiado el código del proyecto y diversos recursos de utilidad.

## Descarga del proyecto

El proyecto se puede descargar mediante dos mecanismos básicos:

1. Si se desea realizar la descarga mediante un cliente git de linea de comandos únicamente debemos teclear la sentencia ***git clone https://bitbucket.org/alfonsomartinmurillo/pfg-amm.git.***
2. Si se desea realizar la descarga directamente en la herramienta Sourcetree ́únicamente deberemos seleccionarlo dentro de la pantalla de clone.

## Ramas del proyecto

El proyecto dispone de las siguientes ramas de trabajo:

* ***Master***. Rama productiva que contiene la última versión operativa del proyecto. 
* ***Develop***. Rama no productiva donde se puede estar trabajando en una futura versión.

Se puede dar el caso de que existan ramas adicionales. Éstas representarán posibles variaciones sobre el proyecto inicial. En cualquier caso, siempre que se desee descargar el proyecto se debe acudir a la rama master. 

## Estructura de directorios

El repositorio dispone de los siguientes directorios:

* ***Análisis y diseño***. En esta carpeta se encuentran elementos relacionados con el análisis del proyecto, y que están disponibles para su consulta:
	* La carpeta “Modelo EA”, que dispone de todos los diagramas y modelos elaborados en una pequeñaa utilidad html que nos permite no solo consultar la información gráfica, si no navegar por los diferentes apartados.
	* El fichero “pfg-amm-analisis.eapx”, que contiene todos los análisis y diseños en formato Enterprise Architect.

* ***Scripts bbdd***. En esta carpeta disponemos del siguiente contenido:

	* El fichero “modelo-fisico.sql” que contiene el script para la creación desde cero de la base de datos.
	* El fichero “utilidades.sql” que contiene consultas elaboradas que pueden resultar de utilidad a la hora de trabajar directamente con la base de datos.

* ***test***. Carpeta donde se ubican ficheros con datos de prueba.
* ***uned-educa-analisis***. Carpeta con todo el proyecto Java. 

## Importar el proyecto en Eclipse

Para importar el proyecto en eclipse se deben de realizar los siguientes pasos:

1. Descargarse el directorio ***“uned-educa-analisis”*** del repositorio, que contiene todo el código java.
2. Acceder al menú “File/Import” y seleccionar la opción “General/Existing Projects into Workspace”.
3. Seleccionar la carpeta de proyecto y finalizar el proceso. Si no se ha copiado la carpeta de proyecto dentro del directorio de workspace, seleccionar la opción de “***Copy projects into workspace***”.





